# Installation
> `npm install --save @types/caniuse-api`

# Summary
This package contains type definitions for caniuse-api (https://github.com/nyalab/caniuse-api#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/caniuse-api.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/caniuse-api/index.d.ts)
````ts
export const features: string[];

export interface BrowserSupport {
    [browser: string]: {
        y?: number | undefined;
        n?: number | undefined;
        a?: number | undefined;
        x?: number | undefined;
        p?: number | undefined;
    };
}

export function getSupport(feature: string): BrowserSupport;

export function isSupported(
    feature: string,
    browsers: string | readonly string[],
): boolean;

export function find(query: string): string[];

export function getLatestStableBrowsers(): string[];

export function setBrowserScope(
    browserscope: string | readonly string[],
): void;

export function getBrowserScope(): string[];

````

### Additional Details
 * Last updated: Mon, 20 Nov 2023 23:36:23 GMT
 * Dependencies: none

# Credits
These definitions were written by [Dave Cardwell](https://github.com/davecardwell), and [Matt Miller](https://github.com/matt123miller).
